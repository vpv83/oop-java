package ru.ccfit.nsu.seminar.upsale;

import ru.ccfit.nsu.seminar.beverage.Beverage;

public class LemonDecorator extends Beverage {

    private Beverage beverage;

    public LemonDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getName() {
        return beverage.getName() + " с лимоном\n";
    }

    @Override
    public double getPrice() {
        return beverage.getPrice() + 10;
    }

    @Override
    public void drink() {
        beverage.drink();
        System.out.println("с лимоном\n");
    }
}
