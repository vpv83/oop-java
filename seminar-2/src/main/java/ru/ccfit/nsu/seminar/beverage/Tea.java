package ru.ccfit.nsu.seminar.beverage;

public class Tea extends Beverage {
    public Tea() {
        name = "Чай";
        price = 100;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void drink() {
        System.out.println("Глоток чая");
    }
}
