package ru.ccfit.nsu.seminar.beverage;

public abstract class Beverage implements Drinkable {

    protected String name;

    protected double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Beverage{" +
               "name='" + name + '\'' +
               '}';
    }
}
