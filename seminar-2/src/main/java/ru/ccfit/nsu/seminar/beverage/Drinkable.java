package ru.ccfit.nsu.seminar.beverage;

public interface Drinkable {
    void drink();
}
