package ru.ccfit.nsu.seminar.beverage;

public class Coffee extends Beverage {

    public Coffee() {
        super();
        name = "Кофе";
        price = 150;
    }

    @Override
    public void drink() {
        System.out.println("Глоток кофе");
    }
}
