package ru.ccfit.nsu.seminar;

import ru.ccfit.nsu.seminar.beverage.Beverage;
import ru.ccfit.nsu.seminar.beverage.Coffee;
import ru.ccfit.nsu.seminar.beverage.Drinkable;
import ru.ccfit.nsu.seminar.upsale.LemonDecorator;
import ru.ccfit.nsu.seminar.upsale.SugarDecorator;

public class Main {

    public static void main(String[] args) {
        Beverage drinkable = new LemonDecorator(
                new SugarDecorator(new SugarDecorator(new Coffee())));
        drinkable.drink();
        System.out.println(drinkable.getPrice());
    }


}
