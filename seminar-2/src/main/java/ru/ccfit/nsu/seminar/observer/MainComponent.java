package ru.ccfit.nsu.seminar.observer;

public class MainComponent {

    public static void main(String[] args) {

        TetrisViewComponent viewComponent = new TetrisViewComponent();

        TetrisGameController tetrisGameController = new TetrisGameController();
        tetrisGameController.register(viewComponent);

        tetrisGameController.gameStart();

        tetrisGameController.gameOver();

    }
}
