package ru.ccfit.nsu.seminar.observer;

import ru.ccfit.nsu.seminar.observer.event.Event;
import ru.ccfit.nsu.seminar.observer.event.GameOverEvent;

public class TetrisViewComponent implements Observer {
    @Override
    public void handle(Event e) {
        if (e instanceof GameOverEvent) {
            System.out.println("Завершение игры. Показываем сообщение пользователю и блокируем экран");
        } else {
            System.out.println("Игнорируем другое событие : " + e.getClass());
        }
    }
}
