package ru.ccfit.nsu.seminar.observer;

import ru.ccfit.nsu.seminar.observer.event.Event;
import ru.ccfit.nsu.seminar.observer.event.GameOverEvent;
import ru.ccfit.nsu.seminar.observer.event.GameStartEvent;

import java.util.ArrayList;
import java.util.List;

public class TetrisGameController extends ObservableImpl {

    /**
     * Завершение игры.
     */
    public void gameOver() {
        // бизнес логика завершения игры
        notify(new GameOverEvent());
    }

    /**
     * Старт игры.
     */
    public void gameStart() {
        notify(new GameStartEvent());
    }
}
