package ru.ccfit.nsu.seminar.observer;

import ru.ccfit.nsu.seminar.observer.event.Event;

public interface Observer {
    /**
     * Обработать событие.
     * @param e
     */
    void handle (Event e);
}
