package ru.ccfit.nsu.seminar.upsale;

import ru.ccfit.nsu.seminar.beverage.Beverage;

public class SugarDecorator extends Beverage {
    private Beverage beverage;

    public SugarDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getName() {
        return beverage.getName() + " с сахаром\n";
    }

    @Override
    public double getPrice() {
        return beverage.getPrice() + 5;
    }

    @Override
    public void drink() {
        beverage.drink();
        System.out.println("с сахаром\n");
    }
}
