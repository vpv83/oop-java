import org.junit.Assert;
import org.junit.Test;
import ru.nsu.lessons.inner.BinaryOperation;
import ru.nsu.lessons.inner.SumOperation;

public class SumOperationTest {

    @Test
    public void sumTest() {
        BinaryOperation operation = new SumOperation();

        Assert.assertEquals(3, operation.operation(1, 2));
    }
}
