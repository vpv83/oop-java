package ru.nsu.lessons.seminar10;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;

public class Counter {

    public static final Logger logger = LoggerFactory.getLogger(Counter.class);
    private int counter = 0;


    synchronized public int increment() {
        counter = counter + 1;
        return counter;
    }


    public static void main(String[] args) {

        Counter c = new Counter();

        Runnable r = () -> {
            while (true) {
                int counter = c.increment();
                logger.info("{} - {}", Thread.currentThread().getId(), counter);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(r).start();
        new Thread(r).start();

    }
}
