package ru.nsu.lessons.inner;

import lombok.Getter;
import lombok.Setter;

public interface BinaryOperation {
    int operation(int op1, int op2);
}
