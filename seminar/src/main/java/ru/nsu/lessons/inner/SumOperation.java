package ru.nsu.lessons.inner;

public class SumOperation implements BinaryOperation{
    @Override
    public int operation(int op1, int op2) {
        return op1 + op2 ;
    }
}
