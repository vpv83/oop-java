package ru.nsu.lessons.seminar3;

public class Seminar3 {

    int x = 0;

    class InnerClass {
        int x = 1;


        public int  displayOuterX() {
            return Seminar3.this.x;
        }
    }

    public static class NestedClass {
        int nestedX = 0;
    }

}
