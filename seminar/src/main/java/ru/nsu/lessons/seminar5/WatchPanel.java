package ru.nsu.lessons.seminar5;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;

public class WatchPanel extends JPanel implements ActionListener {

    private Timer timer = new Timer(1000, this);

    private int shiftForHoursTitle = 20;

    private int radius;

    public WatchPanel(int radius) {
        super();
        this.radius = radius;
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        timer.start();
    }

    private void paintWatch(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;

        int width = getWidth();
        int height = getHeight();

        int leftX = width / 2 - radius;
        int leftY = height / 2 - radius;

        int centerX = leftX + radius;
        int centerY = leftY + radius;

        graphics2D.drawOval(leftX, leftY, 2 * radius, 2 * radius);

        double alpha = 2 * Math.PI / 12;

        for (int i = 0; i < 12; i++) {
            double angle = Math.PI / 2 - i * alpha;
            int xC = (int) ((radius + shiftForHoursTitle) * Math.cos(angle));
            int xY = (int) ((radius + shiftForHoursTitle) * Math.sin(angle));
            graphics2D.drawString(String.valueOf(i), centerX + xC, centerY - xY);
        }


        int second = LocalDateTime.now().getSecond();

        alpha = 2 * Math.PI / 60;
        double angle = Math.PI / 2 - second * alpha;
        int xC = (int) (radius * Math.cos(angle));
        int xY = (int) (radius * Math.sin(angle));
        graphics2D.drawLine(centerX, centerY, centerX + xC, centerY - xY);


    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        paintWatch(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }
}
