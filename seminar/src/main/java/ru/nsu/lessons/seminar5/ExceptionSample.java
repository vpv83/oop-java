package ru.nsu.lessons.seminar5;

public class ExceptionSample {

    static class MyException extends Exception {

    }


    public static void main(String[] args) {

        try {
            doSmth();
        } catch (RuntimeException e) {

        } catch (Exception e) {
        }

        Runnable r = () -> {
            try {
                Thread.sleep(1000l);
                System.out.println(Thread.currentThread() + " finish");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        for (int i = 0; i < 10; i++)
            new Thread(r).start();

        System.out.println(Thread.currentThread() + " finish");

    }

    /**
     * @throws MyException
     */
    public static void doSmth() throws MyException {
        throw new MyException();
    }
}
