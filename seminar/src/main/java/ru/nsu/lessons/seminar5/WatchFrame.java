package ru.nsu.lessons.seminar5;

import javax.swing.JFrame;
import java.awt.Dimension;

public class WatchFrame extends JFrame {

    public static void main(String[] args) {
        WatchFrame watch = new WatchFrame();
        watch.setSize(new Dimension(800, 600));
        watch.setTitle("Часы");
        watch.getContentPane().add(new WatchPanel(200));
        watch.setVisible(true);
        watch.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

}
