package ru.nsu.lessons.seminar9;

public class AdapterImpl implements AddNumber {

    private AddNumber addNumber;

    public AdapterImpl(AddNumber addNumber) {
        this.addNumber = addNumber;
    }

    public int add(int a, int b) {
        return 2 * addNumber.add(a / 2, b / 2);
    }
}


interface AddNumber {
    int add(int a, int b);
}

class AddNumberImpl implements AddNumber {
    @Override
    public int add(int a, int b) {
        return a + b;
    }
}
