package ru.nsu.lessons.seminar9;

public class ProxyCommonInterfaceImpl implements CommonInterface {

    private CommonInterfaceImpl commonInterface;


    public ProxyCommonInterfaceImpl(CommonInterfaceImpl commonInterface) {
        this.commonInterface = commonInterface;
    }

    @Override
    public void doSmth() {
        // добавить все что нужно
        commonInterface.doSmth();
    }
}
