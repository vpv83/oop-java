package ru.nsu.lessons.seminar11;

import lombok.Data;

public class Multithreading {

    public static Object synchronizeObject = new Object();

    @Data
    public static class Product {
        private String id;
    }

    public static void main(String[] args) {

        Storage storage = new Storage();
        Thread t1 = new Thread(new Producer(storage));
        Thread t2 = new Thread(new Consumer(storage));

        t1.start();
        t2.start();


    }
}
