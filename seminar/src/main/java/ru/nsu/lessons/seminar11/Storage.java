package ru.nsu.lessons.seminar11;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private List<Multithreading.Product> list = new ArrayList<>();

    public void add(Multithreading.Product p) {
        list.add(p);
    }

    public Multithreading.Product get() {
        return list.remove(0);
    }

    synchronized
    public int size() {
        return list.size();
    }

}
