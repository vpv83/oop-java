package ru.nsu.lessons.seminar11;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer implements Runnable {

    private Storage storage;
    private Logger logger = LoggerFactory.getLogger(Consumer.class);

    public Consumer(Storage storage) {
        this.storage = storage;
    }

    public void consume() throws InterruptedException {
        while (true) {
            synchronized (Multithreading.synchronizeObject) {
                if (storage.size() > 0) {
                    logger.info("Берем продукт со склада.");
                    Multithreading.Product product = storage.get();
                    logger.info("Потребитель будит остальные потоки..");
                    Multithreading.synchronizeObject.notifyAll();
                } else {
                    logger.info("Продуктов нет. Спим.");
                    Multithreading.synchronizeObject.wait();
                }
            }

            try {
                Thread.sleep(2000l);
            } catch (InterruptedException e) {

            }
        }
    }

    @Override
    public void run() {
        try {
            consume();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
