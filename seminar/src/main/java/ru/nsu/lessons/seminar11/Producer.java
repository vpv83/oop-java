package ru.nsu.lessons.seminar11;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Producer implements Runnable {

    public static Logger logger = LoggerFactory.getLogger(Producer.class);

    private Storage storage;

    public Producer(Storage storage) {
        this.storage = storage;
    }

    public void produce() throws InterruptedException {
        while (true) {
            synchronized (Multithreading.synchronizeObject) {
                if (storage.size() < 5) {
                    logger.info("Произведен новый продукт");
                    storage.add(new Multithreading.Product());
                    logger.info("Производитель будит остальные потоки..");
                    Multithreading.synchronizeObject.notifyAll();
                } else {
                    logger.info("Хранилище переполнено. Спим.");
                    Multithreading.synchronizeObject.wait();
                }
            }
            try {
                Thread.sleep(1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        try {
            produce();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
