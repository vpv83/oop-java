package ru.nsu.lessons.seminar4;

public class SumOperation implements BinaryOperation{

    @Override
    public int apply(int op1, int op2) {
        return op1 + op2;
    }
}
