package ru.nsu.lessons.seminar4;

public interface BinaryOperation {

    int apply(int op1, int op2);

}
