package ru.nsu.lessons.seminar4;

import java.util.HashSet;
import java.util.Set;

public class Test {

    static class InstanceClass {
        private int a = 0;

        public InstanceClass(int a) {
            this.a = a;
        }

        @Override
        public boolean equals(Object obj) {
            InstanceClass instanceClass = (InstanceClass) obj;
            return this.a == instanceClass.a;
        }

        @Override
        public int hashCode() {
            System.out.println("hashCode()");
            return a;
        }
    }

    public static void main(String[] args) {
        InstanceClass instanceClass = new InstanceClass(1);

        Set set = new HashSet();
        set.add(instanceClass);

        findIdentity(set);
    }

    public static void findIdentity(Set set) {
        InstanceClass identity = new InstanceClass(1);
        boolean r = set.contains(identity);
        System.out.println(r);
    }

}
