package ru.nsu.lessons.seminar12;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CollectionTest {

    public static Logger logger = LoggerFactory.getLogger(CollectionTest.class);


    static class Car {

        int id;

        String model;

        int productionYear;

        public Car(int id, String model, int productionYear) {
            this.id = id;
            this.model = model;
            this.productionYear = productionYear;
        }


        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public int getProductionYear() {
            return productionYear;
        }

        public void setProductionYear(int productionYear) {
            this.productionYear = productionYear;
        }


        @Override
        public String toString() {
            return "Car{" +
                    "id=" + id +
                    ", model='" + model + '\'' +
                    ", productionYear=" + productionYear +
                    '}';
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Car == false) {
                return false;
            }

            Car c = (Car) obj;

            return c.getModel().equals(model) &&
                    c.getProductionYear() == productionYear
                    && c.getId() == id;
        }
    }

    public static void main(String[] args) {
        Map<String, Car> cars = new TreeMap<>();


        cars.put("light-1", new Car(1, "тойота", 1999));
        cars.put("light-2", new Car(2, "ниссан", 2000));
        cars.put("heavy-1", new Car(3, "зил", 1993));
        cars.put("heavy-2", new Car(4, "камаз", 1994));


        for (Map.Entry<String, Car> entry : cars.entrySet()) {
            logger.info("{} - {}", entry.getKey(), entry.getValue());
        }


    }
}
