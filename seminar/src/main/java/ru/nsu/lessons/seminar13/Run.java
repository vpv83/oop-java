package ru.nsu.lessons.seminar13;

import java.util.ArrayList;
import java.util.List;

public class Run {

    public static final int HANDLERS_NUMBER = 2;

    public static void main(String[] args) throws InterruptedException {

        List<Message> messages = new ArrayList<>();

        for (int i = 0 ; i < HANDLERS_NUMBER; i++) {
            new Thread(new MessageHandler(messages))
                    .start();
        }

        new MessageSender(messages).send();
    }
}
