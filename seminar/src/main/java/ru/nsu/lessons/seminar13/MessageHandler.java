package ru.nsu.lessons.seminar13;

import java.util.List;

public class MessageHandler implements Runnable {

    private List<Message> messages;

    public MessageHandler(List<Message> messages) {
        this.messages = messages;
    }

    public void handle() throws InterruptedException {

        Message m = null;
        while (true) {
            synchronized (messages) {
                while (messages.size() <= 0) {
                    System.out.println(Thread.currentThread() + ": Нового сообщения нет. Засыпаем");
                    messages.wait();
                }

                m = messages.remove(0);
            }
            processMessage(m);
        }
    }


    private void processMessage(Message message) throws InterruptedException {

        System.out.println(Thread.currentThread() + ": Получено новое сообщение. Обработка.");
        String text = message.getText();
        String[] str = text.split(" ");

        for (String s : str) {
            System.out.println(Thread.currentThread() + ": " + s);
        }
        Thread.sleep(10000l);
    }

    @Override
    public void run() {
        try {
            handle();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
