package ru.nsu.lessons.seminar13;

import java.util.List;
import java.util.Scanner;

public class MessageSender {

    private List<Message> messages;

    private Scanner scanner;

    public MessageSender(List<Message> messages) {
        this.messages = messages;
        scanner = new Scanner(System.in);
    }

    public void send() throws InterruptedException {
        while (true) {
            String message = scanner.nextLine();

            synchronized (messages) {
                System.out.println(Thread.currentThread() + ": Отправьте новое сообщение...");
                messages.add(new Message(message));
                messages.notifyAll();
            }

            Thread.sleep(1000l);
        }
    }
}
