package ru.nsu.lessons.seminar2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Objects;

public class Collection {

    private static final Logger logger = LoggerFactory.getLogger(Collection.class);

    interface Calculator {
        int sum(int a1, int a2); // == public abstract int sum (...)
        double multiply(int a1, int a2);
    }

    abstract class AbstractCalculator implements Calculator { // extends Object

        public abstract int sum(int a1, int a2);

        public double multiply (int a1, int a2) {
            return a1 * a2;
        }
    }

    class CalculatorImpl extends AbstractCalculator {

        @Override
        public int sum(int a1, int a2) {
            return a1 + a2;
        }
    }

    static abstract class PersistentEntity {

        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

    static class Student extends PersistentEntity {

    }


    static class Person extends PersistentEntity {
        private String name;

        @Override
        public boolean equals(Object obj) {

            if (obj instanceof Person == false) {
                return false;
            }

            return Objects.equals(this.name, ((Person) obj).name);
        }

        @Override
        public String toString() {
            return "Person {" +
                    "name='" + name + '\'' +
                    '}';
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


    public static void main(String[] args) {

        Person p1 = new Person();
        p1.setName("Иванов");
        Person p2 = new Person();
        p2.setName("Иванов");

        // o1 == o2 => o1.hashCode() == o2.hashCode()
        // o1.hashCode() == o2.hashCode => o1 == o2 НЕ ВЕРНО!

        Integer i1 = 1;
        Integer i2 = 1;

        System.out.println(i1 == i2);

        i1 = 500;
        i2 = 500;
        System.out.println(i1 == i2);

//
//        logger.info("Person: {}", p1);
//        // boolean cmp = Objects.equals(p1, p2);
//        logger.debug("Результат = {}", p1.equals(p2));
//
//        p1.getClass().getConstructors();
    }

}
