package ru.nsu.lessons.seminar6;

import ru.nsu.lessons.seminar6.controller.TimerUpdateController;
import ru.nsu.lessons.seminar6.model.TimerModel;
import ru.nsu.lessons.seminar6.view.TimerPanel;

import javax.swing.JFrame;
import java.awt.Dimension;

public class TimerApp extends JFrame {

    public static void main(String[] args) {
        TimerApp watch1 = new TimerApp();
        watch1.setSize(new Dimension(800, 600));
        watch1.setTitle("Часы");
        TimerModel timer = new TimerModel();

        TimerPanel timerPanel = new TimerPanel(200, timer);
        watch1.getContentPane().add(timerPanel);

        TimerUpdateController controller = new TimerUpdateController(timerPanel, timer, 1000);
        watch1.setVisible(true);
        watch1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

}
