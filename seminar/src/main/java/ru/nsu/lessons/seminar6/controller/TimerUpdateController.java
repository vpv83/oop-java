package ru.nsu.lessons.seminar6.controller;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.lessons.seminar6.event.WatchUpdateEvent;
import ru.nsu.lessons.seminar6.model.TimerModel;
import ru.nsu.lessons.seminar6.service.Observable;
import ru.nsu.lessons.seminar6.view.TimerPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Slf4j
/**
 * Контроллер обновление времени по таймеру.
 */
public class TimerUpdateController extends Observable implements ActionListener  {
    private TimerPanel watch;
    private TimerModel timerModel;
    private javax.swing.Timer timer;


    public TimerUpdateController(TimerPanel watch, TimerModel timerModel, int delay) {
        this.watch = watch;
        this.timerModel = timerModel;
        // добавляем объект, которые будет реагировать на событие обновления времени
        addObserver(watch);
        this.timer = new javax.swing.Timer(delay, this);
        this.timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        log.info("Controller update()..");
        notify(new WatchUpdateEvent());
    }
}

