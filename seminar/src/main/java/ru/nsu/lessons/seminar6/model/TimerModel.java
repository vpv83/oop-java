package ru.nsu.lessons.seminar6.model;

import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.LocalDateTime;

@Slf4j
/**
 * Модель времени.
 */
public class TimerModel {

    private LocalDateTime start = LocalDateTime.now();

    public TimerModel() {

    }

    public int getSeconds() {
        Duration duration = Duration.between(start, LocalDateTime.now());
        return duration.toSecondsPart();
    }

    // .. остальные методы
}
