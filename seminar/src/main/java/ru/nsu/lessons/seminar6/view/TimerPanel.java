package ru.nsu.lessons.seminar6.view;

import ru.nsu.lessons.seminar6.event.Event;
import ru.nsu.lessons.seminar6.event.WatchUpdateEvent;
import ru.nsu.lessons.seminar6.service.Observer;
import ru.nsu.lessons.seminar6.model.TimerModel;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.text.DecimalFormat;

public class TimerPanel extends JPanel implements Observer {

    private int shiftForHoursTitle = 20;

    private int radius;

    private TimerModel timer;

    public TimerPanel(int radius, TimerModel timer) {
        super();
        this.radius = radius;
        this.timer = timer;

        setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    private void paintWatch(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;

        int width = getWidth();
        int height = getHeight();

        int leftX = width / 2 - radius;
        int leftY = height / 2 - radius;


        // рисуем каркас часов
        drawWatchFoundation(graphics2D, leftX, leftY, radius);

        /**
         * Рассчитывам и рисуем текущее время
         */
        int centerX = leftX + radius;
        int centerY = leftY + radius;
        double alpha = 2 * Math.PI / 60;
        double angle = Math.PI / 2 - timer.getSeconds() * alpha;
        int xC = (int) (radius * Math.cos(angle));
        int xY = (int) (radius * Math.sin(angle));
        graphics2D.drawLine(centerX, centerY, centerX + xC, centerY - xY);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        paintWatch(g);


        int min = timer.getSeconds() / 60;
        int seconds = timer.getSeconds() % 60;
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setColor(Color.RED);
        graphics2D.setFont(new Font("Arial", Font.PLAIN, 30));
        graphics2D.drawString(String.format("%02d:%02d", min, seconds), 100, 100);
    }

    private void drawWatchFoundation(Graphics2D graphics2D, int leftX, int leftY, int radius) {

        graphics2D.drawOval(leftX, leftY, 2 * radius, 2 * radius);

        int centerX = leftX + radius;
        int centerY = leftY + radius;

        double alpha = 2 * Math.PI / 12;

        for (int i = 0; i < 12; i++) {
            double angle = Math.PI / 2 - i * alpha;
            int xC = (int) ((radius + shiftForHoursTitle) * Math.cos(angle));
            int xY = (int) ((radius + shiftForHoursTitle) * Math.sin(angle));
            graphics2D.drawString(String.valueOf(i), centerX + xC, centerY - xY);
        }

    }

    @Override
    public void notify(Event event) {
        if (event instanceof WatchUpdateEvent) {
            repaint();
        }
    }
}
