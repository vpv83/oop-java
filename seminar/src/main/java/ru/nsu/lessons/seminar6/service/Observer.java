package ru.nsu.lessons.seminar6.service;

import ru.nsu.lessons.seminar6.event.Event;

public interface Observer {
    void notify(Event event);
}
