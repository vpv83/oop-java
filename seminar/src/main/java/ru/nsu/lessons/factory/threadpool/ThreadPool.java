package ru.nsu.lessons.factory.threadpool;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool {

    private int size;

    private List<Thread> threadList;

    public ThreadPool(int size) {
        this.size = size;
    }

    public void execute(Runnable r) {
        if (threadList.size() < size) {
            Thread thread = new Thread(() -> {
                r.run();
                threadList.remove(this);
            });
            threadList.add(thread);
            thread.start();
        } else {
            throw new RuntimeException("Пул потоков переполнен");
        }
    }
}
